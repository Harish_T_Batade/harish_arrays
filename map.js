function map(elements, cb) {
    // Do NOT use .map, to complete this function.
    // How map works: Map calls a provided callback function once for each element in an array, in order, and functionructs a new array from the res .
    // Produces a new array of values by mapping each value in list through a transformation function (iteratee).
    // Return the new array.
    let new_mapped_array=[];
    if(Array.isArray(elements)==true)
    {
        for (let index = 0; index < elements.length; index++){     // loop tp call the callback funtion for each element
            let element = elements[index];

            new_mapped_array.push(square(element,index));   
        }
    }
    else
    {
        return "function only works on Arrays";
    }
    return new_mapped_array;

}

function square (element,index)
{
    element=Math.pow(element,2);

    return element;
}


module.exports = {map , square};