const nestedArray = [1, [2], [[3]], [[[4]]]];

const testing_flatten = require('./flatten')

console.log(testing_flatten.flatten(nestedArray));

function testFlatten(nestedArray) {
    return testing_flatten.flatten(nestedArray);
}