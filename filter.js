function filter(elements, filtering) {
    // Do NOT use .filter, to complete this function.
    // Similar to `find` but you will return an array of all elements that passed the truth test
    // Return an empty array if no elements pass the truth test
    
    let filterd_Array = [];
    if(Array.isArray(elements)==true && elements.length!=0)
    {
        for (let index = 0; index < elements.length; index++) {        // loop tp call the callback funtion for each element
        
            if(filtering(elements[index]))
            {
                filterd_Array.push(elements[index]);
            }
        }
    }
    else
    {
        return "function only works on Arrays with atleast one element";
    }

    if(filterd_Array.length==0)
    {
        return "There are no elements matching the test in the given array"
    }
    return filterd_Array;
}

function filtering(element)
{
    if(element>4)
    {
        return true;
    }
    else
    {
        return false;
    }
}

module.exports = { filter, filtering };