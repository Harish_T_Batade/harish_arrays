function each(elements, callback) {
    
    let new_element_array=[];
    if(Array.isArray(elements)==true)
    {
        for (let index = 0; index < elements.length; index++){    // loop tp call the callback funtion for each element
            let element = elements[index];

            new_element_array.push(callback(element,index));   
        }
    }
    else
    {
        return "function only works on Arrays";
    }
    return new_element_array;
}

// incrementing each element by five

function callback (element,index)
{
    element=element+5;

    return element;
}


module.exports = {each , callback};